/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 c-style: "K&R" -*- */

/*---------------------------------------------------------------------------
   t-avg-img: calculates time-averaged values from a series if images

   Copyright (C) 2002, 2003, 2004, 2005, 2006, 2007, 2008
   Gerber van der Graaf <gerber_graaf@users.sourceforge.net

   T-avg-img is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  



TODO:

BUGS:

------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <gpiv.h>
#include "config.h"
#include "git-rev.h"

#define PARFILE "t-avg-img.par"	/* Parameter file name */
#define USAGE "\
Usage: gpiv_t-avg-img [-x | --prefix] [-b | --basename file] [-f | --first int] \
[l | --last int] [-h | --help] [-p | --print] \n\
[-s | --subtr] [-v | --version] \n\
\n\
keys: \n\
\n\
-b | --basename FILE:      file base-name (without .number.r extension) \n\
                           instead of stdin and stdout \n\
-f | --first N:            number of first image file \n\
-h | --help:               this on-line help \n\
-l | --last N:             number of last image file \n\
-p | --print:              print parameters to stdout \n\
-s | --subtr:              subtract mean from input images  \n\
-v | --version:            version number \n\
-x | --prefix:             prefix numbering to file base name \n\
"

#define HELP  "\
Calculates time-averaged intensities from a series of images at each pixel. \n\
Images should be numbered at the start or at the end of its name"

/*---------- Global variables ---------------------------------------------*/
gboolean fname__set = FALSE;
gboolean verbose = FALSE;


/*
 * Function prototypes 
 */

void 
command_args (int argc, char *argv[], 
             char fname[GPIV_MAX_CHARS],
             GpivGenPar *gen_par,
             GpivImageProcPar *image_proc_par
             )
/* ----------------------------------------------------------------------------
 * Command line argument handling
 */
{
    char c;
    int argc_next;
    while (--argc > 0 && (*++argv)[0] == '-') {
	argc_next = 0;
        /*
         * argc_next is set to 1 if the next cmd line argument has to
         * be searched for; in case that the command line argument
         * concerns more than one char or cmd line argument needs a
         * parameter
         */
	while (argc_next == 0 && (c = *++argv[0])) {
	    switch (c) {
	    case 'v':
                /*
                 * Use Git revision control system
                 */
#ifdef GIT_HASH
                printf ("git hash: %s\n", GIT_REV);
#else
                printf ("version: %s\n", GPIVTOOLS_VERSION);
#endif
		exit(0);
		break;
	    case 'h':
		printf("\n%s", argv[0]);
		printf("\n%s", HELP);
		printf("\n%s", USAGE);
		exit(0);
		break;

	    case 'p':
                verbose = TRUE;
		break;
                /*
                 * file name and numbers
                 */
	    case 'b':
		strcpy(fname, *++argv);
		fname__set = TRUE;
		argc_next = 1;
		--argc;
		break;

	    case 'f':
                gen_par->first_file = atoi(*++argv);
                gen_par->first_file__set = TRUE;
                --argc;
                argc_next = 1;
		break;

	    case 'l':
                gen_par->last_file = atoi(*++argv);
                gen_par->last_file__set = TRUE;
                --argc;
                argc_next = 1;
		break;

	    case 'x':
                gen_par->file_prefix = TRUE;
		break;
                /*
                 * subtract mean from input data
                 */
	    case 's':
		    image_proc_par->smooth_operator = GPIV_IMGOP_SUBTRACT;
		break;
                /*
                 * long option keys
                 */
	    case '-':
		if (strcmp("-help", *argv) == 0) {
                    printf("\n%s", argv[0]);
                    printf("\n%s", HELP);
                    printf("\n%s", USAGE);
                    exit(0);
                } else if (strcmp("-print", *argv) == 0) {
		    verbose = TRUE;
                } else if (strcmp("-basename", *argv) == 0) {
                    strcpy(fname, *++argv);
                    fname__set = TRUE;
                    argc_next = 1;
                    --argc;
                } else if (strcmp("-first", *argv) == 0) {
                    gen_par->first_file = atoi(*++argv);
                    gen_par->first_file__set = TRUE;
                    --argc;
                    argc_next = 1;
                } else if (strcmp("-last", *argv) == 0) {
                    gen_par->last_file = atoi(*++argv);
                    gen_par->last_file__set = TRUE;
                    --argc;
                    argc_next = 1;
                } else if (strcmp("-subtr", *argv) == 0) {
 		    image_proc_par->smooth_operator = GPIV_IMGOP_SUBTRACT;
                } else if (strcmp("-prefix", *argv) == 0) {
                    gen_par->file_prefix = TRUE;
                }

	    default:
		gpiv_error("%s: unknown option: %s", argv[0], *argv);
		break;
	    }
	}
    }

    if (argc != 0) {
	gpiv_error("%s: unknown argument: %s", argv[0], *argv);
    }


}



void 
make_fname_out (char *argv[],
                gchar *fname_base, 
                gchar *fname_header, 
                gchar *fname_parameter,
                gchar *fname_out
                )
/*-----------------------------------------------------------------------------
 * generates filenames for output
 */
{

  char f_dum[GPIV_MAX_CHARS];
#ifdef GIT_HASH
    if (fname__set == FALSE) 
        gpiv_error("Software: %s git hash: %s: File basename has to be set", 
                   argv[0], GIT_REV);
#else
    if (fname__set == FALSE) 
        gpiv_error("Software: %s version: %s: File basename has to be set", 
                   argv[0], GPIVTOOLS_VERSION);
#endif

    gpiv_io_make_fname(fname_base, GPIV_EXT_HEADER, fname_header);
    if (verbose) printf("# Data parameter file: %s\n", fname_header);

    gpiv_io_make_fname(fname_base, GPIV_EXT_PAR, fname_parameter);
    if (verbose) printf("# Data parameter file: %s\n", fname_parameter);

    gpiv_io_make_fname(fname_base, GPIV_EXT_TA, f_dum);
    gpiv_io_make_fname(f_dum, GPIV_EXT_PNG_IMAGE, fname_out);
    if (verbose) printf("# Output file: %s\n", fname_out);

}



void 
make_fname_in (char *argv[],
               GpivGenPar *gen_par,
	       gchar *fname_base, 
	       int f_number, 
               gchar *fname_in
               )
/*-----------------------------------------------------------------------------
 * generates filenames for input
 */
{
  char f_dum[GPIV_MAX_CHARS];
#ifdef GIT_HASH
  if (fname__set == FALSE) 
      gpiv_error("Software: %s git hash: %s: File basename has to be set", 
                 argv[0], GIT_REV);
#else
  if (fname__set == FALSE) 
      gpiv_error("Software: %s version: %s: File basename has to be set", 
                 argv[0], GPIVTOOLS_VERSION);
#endif

    if (gen_par->file_prefix) {
        snprintf(f_dum, GPIV_MAX_CHARS, "%d%s", f_number, fname_base);
        gpiv_io_make_fname(f_dum, GPIV_EXT_RAW_IMAGE, fname_in);
    } else {
        snprintf(f_dum, GPIV_MAX_CHARS, "%s%d", fname_base, f_number);
        gpiv_io_make_fname(f_dum, GPIV_EXT_RAW_IMAGE, fname_in);
    }
      
    if (verbose) printf("# Input file: %s\n", fname_in);
     fprintf(stderr, "MAKE_FNAME_IN:: leaving\n");

}



GpivImage *
imgproc_mean (char *argv[],
              GpivGenPar *gen_par, 
              GpivImageProcPar *image_proc_par, 
              gchar *fname_base
              )
/*-----------------------------------------------------------------------------
 */
{
     guint j, k, l;
     guint first_file = gen_par->first_file;
     guint last_file = gen_par->last_file;

    gchar fname_in[GPIV_MAX_CHARS];

     GpivImage *image_sum = NULL, *image_mean = NULL, *image_in = NULL;


     if (verbose) printf ("# Calculating mean from input data\n");
     
     for (j = first_file; j <= last_file; j++) {
         make_fname_in (argv, gen_par, fname_base, j, fname_in);
         if ((image_in = gpiv_fread_image (fname_in)) == NULL) {
             gpiv_error ("local_image_mean: failing gpiv_fread_image %s", 
                         fname_in);
         }         
/*
 * Allocate image_sum when image dimensions are known
 * Check image dimensions for all subsequent images
 */
         if (j == first_file) {
             if ((image_sum = gpiv_alloc_img (image_in->header)) == NULL) {
                 gpiv_error ("local_image_mean: failing gpiv_alloc_img for image_sum");
             }
         } else {
             if (image_in->header->ncolumns != image_sum->header->ncolumns
                 || image_in->header->nrows != image_sum->header->nrows
                 || image_in->header->depth != image_sum->header->depth
                 || image_in->header->x_corr != image_sum->header->x_corr
                 ) {
                 gpiv_error ("local_image_mean: in-and output images are of different dimensions");
             }
         }

/*
 * Summing image values at each pixel
 */
	 for (k = 0; k < image_in->header->nrows; k++) {
             for (l = 0; l < image_in->header->ncolumns; l++) {
                 image_sum->frame1[k][l] += image_in->frame1[k][l];
                 if (image_sum->header->x_corr == TRUE) {
                     image_sum->frame2[k][l] += image_in->frame2[k][l];
                 }
             }
	 }
     }


/*
 * Calculate average value at each pixel
 */
     if ((image_mean = gpiv_alloc_img (image_sum->header)) == NULL) {
         gpiv_error ("local_image_mean: failing gpiv_alloc_img for image_mean");
     }

     for (k = 0; k < image_in->header->nrows; k++) {
	 for (l = 0; l < image_in->header->ncolumns; l++) {
             gfloat mean = 0.0;
             mean = (gfloat) image_sum->frame1[k][l] / 
                 (gfloat) (last_file - first_file + 1);
             image_mean->frame1[k][l] = (guint16) mean;
         }
     }

     if (image_sum->header->x_corr == TRUE) {
         for (k = 0; k < image_in->header->nrows; k++) {
             for (l = 0; l < image_in->header->ncolumns; l++) {
                 gfloat mean = 0.0;
                 mean = (gfloat) image_sum->frame2[k][l] / 
                     (gfloat) (last_file - first_file + 1);
                 image_mean->frame2[k][l] = (guint16) mean;
             }
	 }
     }


     gpiv_free_img (image_in);
     gpiv_free_img (image_sum);
     return image_mean;
}




static gchar *
imgproc_subtract (char *argv[],
                  GpivGenPar *gen_par, 
                  gchar *fname_base,
                  GpivImage *image_subtr
                  )
/*-----------------------------------------------------------------------------
 */
{
    gchar *err_msg = NULL;
    FILE *fp = NULL;
    gchar f_dum1[GPIV_MAX_CHARS] = "";
    gchar f_dum2[GPIV_MAX_CHARS] = "";
    gchar fname_out[GPIV_MAX_CHARS] = "";

    guint j, k, l;
/*     guint16 **img_in1 = NULL, **img_in2 = NULL; */
    guint first_file = gen_par->first_file;
    guint last_file = gen_par->last_file;

    guint nrows = image_subtr->header->nrows;
    guint ncolumns = image_subtr->header->ncolumns;
    gchar fname_in[GPIV_MAX_CHARS];

    GpivImage *image_in = NULL;


    if (verbose) printf ("# Subtracting mean from input images\n");

    for (j = first_file; j <= last_file; j++) {

        if (fname__set == TRUE) {
            make_fname_in (argv, gen_par, fname_base, j, fname_in);
            if ((image_in = gpiv_fread_image (fname_in)) == NULL) {
                err_msg = "imgproc_subtract: failing gpiv_fread_image";
                return (err_msg);
            }
        } else {
            if ((image_in = gpiv_read_png_image (stdin)) == NULL) {
                err_msg = "imgproc_subtract: failing gpiv_read_png_image";
                return (err_msg);
            }
        }
         

        if ((err_msg = gpiv_imgproc_subtractimg (image_subtr, image_in)) != NULL) {
                return (err_msg);
            }
            

        if (fname__set == TRUE) {
            snprintf (f_dum1, GPIV_MAX_CHARS, "%s%d", fname_base, j);
            gpiv_io_make_fname (f_dum1, GPIV_EXT_TA, f_dum2);
            gpiv_io_make_fname (f_dum2, GPIV_EXT_PNG_IMAGE, fname_out);
            if ((fp = fopen (fname_out, "wb")) == NULL) {
                err_msg = "img_subtract: unable to open file";
                return (err_msg);
            }
            gpiv_write_png_image (fp, image_in, TRUE);
            fclose (fp);

            if (verbose) printf ("# Output file: %s\n", fname_out);
            snprintf (f_dum1, GPIV_MAX_CHARS, " ");;
            snprintf (f_dum2, GPIV_MAX_CHARS, " ");;
            snprintf (fname_out, GPIV_MAX_CHARS, " ");;

        } else {
            gpiv_write_png_image (stdout, image_in, TRUE);
        }

    }
    
    return (err_msg);
}



int 
main (int argc, 
      char *argv[]
      )
/* ----------------------------------------------------------------------------
 * main routine to calculates time-averaged image intensity
 */
{
    FILE *fp = NULL, *fp_par_dat = NULL;
    gchar fname_base[GPIV_MAX_CHARS], 
        fname_header[GPIV_MAX_CHARS],
        fname_out[GPIV_MAX_CHARS],
	fname_parameter[GPIV_MAX_CHARS];
    GpivImage *image_mean = NULL;
    GpivGenPar *gen_par = g_new0 (GpivGenPar, 1);
    GpivImageProcPar *image_proc_par = g_new0 (GpivImageProcPar, 1);


/*
 * Image processing parameter initialization
 */
    gpiv_genpar_parameters_set (gen_par, FALSE);
    gpiv_imgproc_parameters_set (image_proc_par, FALSE);
    command_args (argc, argv, fname_base, gen_par, image_proc_par);
    if (verbose) {
#ifdef GIT_HASH
        printf ("# Software: %s\n# git hash: %s\n# Command line options:\n", 
                argv[0], GIT_REV);
#else
        printf ("# Software: %s\n# version: %s\n# Command line options:\n", 
                argv[0], GPIVTOOLS_VERSION);
#endif
        gpiv_genpar_print_parameters (stdout, gen_par);
        gpiv_imgproc_print_parameters (stdout, image_proc_par);
    }


    if (fname__set == TRUE) {
/* 
 * Generating proper filenames 
 */
	make_fname_out (argv, fname_base, fname_header, fname_parameter, fname_out);
	if (verbose) 
            printf ("\n# Parameters written to: %s", fname_parameter);


/* 
 * Prints command line parameters to par-file 
 */
	if ((fp_par_dat = fopen (fname_parameter, "a")) == NULL) {
	    gpiv_error ("%s: failure opening %s for input",
                       argv[0], fname_parameter);
	}
	fprintf (fp_par_dat, "\n\n# %s\n# Command line options:\n", argv[0]);
	gpiv_genpar_print_parameters (fp_par_dat, gen_par);
	gpiv_imgproc_print_parameters (fp_par_dat, image_proc_par);

/*
 *  Reading parametes from PARFILE (and writing to data par-file)
 */
        gpiv_scan_parameter (GPIV_GENPAR_KEY, PARFILE, gen_par, verbose);
        gpiv_scan_resourcefiles (GPIV_GENPAR_KEY, gen_par, verbose);
        gpiv_genpar_print_parameters (fp_par_dat, gen_par);

        gpiv_scan_parameter (GPIV_IMGPROCPAR_KEY, PARFILE, image_proc_par, 
			     verbose);
        gpiv_scan_resourcefiles (GPIV_IMGPROCPAR_KEY, image_proc_par, verbose);
        gpiv_imgproc_print_parameters (fp_par_dat, image_proc_par);
	fclose (fp_par_dat);


    } else {
#ifdef GIT_HASH
        gpiv_error("Software: %s git hash: %s: File basename has to be set", 
                   argv[0], GIT_REV);
#else
        gpiv_error("Software: %s version: %s: File basename has to be set", 
                   argv[0], GPIVTOOLS_VERSION);
#endif
    }


/*
 * Check parameters on correct values and adjust belonging  variables
 * Nothing to do here
 */


/*
 * Here the function calls of imgproc_mean and imgproc_subtract
 */
    if ((image_mean = imgproc_mean (argv, gen_par, image_proc_par, fname_base)) 
        == NULL) {
        gpiv_error ("%s: failing img_mean", argv[0]);
    }

    if (image_proc_par->smooth_operator == GPIV_IMGOP_SUBTRACT)
        imgproc_subtract (argv, gen_par, fname_base, image_mean);


/* 
 * And writing to output 
 */
    if (fname__set == TRUE) {
        if ((fp = fopen (fname_out, "wb")) == NULL) {
            return (1);
        }
        gpiv_write_png_image (fp, image_mean, TRUE);
        fclose (fp);
    } else {
#ifdef GIT_HASH
        gpiv_error("Software: %s git hash: %s: File basename has to be set", 
                   argv[0], GIT_REV);
#else
        gpiv_error("Software: %s version: %s: File basename has to be set", 
                   argv[0], GPIVTOOLS_VERSION);
#endif
    }

    return 0;
}
