if(USE_CAM)
add_subdirectory (dac)
endif()
add_subdirectory (image)
add_subdirectory (evaluate)
add_subdirectory (validate)
add_subdirectory (misc)
add_subdirectory (post)
