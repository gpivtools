cmake_minimum_required(VERSION 2.8.0)

project (GPIVTOOLS)
set (GPIVTOOLS_VERSION_MAJOR 0)
set (GPIVTOOLS_VERSION_MINOR 7)
set (GPIVTOOLS_VERSION_PATCH 0)
set (GPIVTOOLS_VERSION 
  ${GPIVTOOLS_VERSION_MAJOR}.${GPIVTOOLS_VERSION_MINOR}.${GPIVTOOLS_VERSION_PATCH})

# add binary directory as include directory
include_directories(${CMAKE_CURRENT_BINARY_DIR})

# set up custom cmake module path
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/CMake/Modules)

# include utilities
include(${CMAKE_SOURCE_DIR}/CMake/GpivUtilities.cmake)

set( CMAKE_VERBOSE_MAKEFILE on )

find_package(Gpiv REQUIRED)
IF(NOT GPIV_FOUND)
MESSAGE(FATAL_ERROR "Could not find Gpiv library")
ENDIF()
#message (Gpiv_INCLUDE_DIR = ${GPIV_INCLUDE_DIR} )
include_directories( ${GPIV_INCLUDE_DIR} )
set( LIBS ${LIBS} ${Gpiv_LIBRARIES} )

find_package(GSL REQUIRED)
#include_directories( ${Gsl_INCLUDE_DIR} )
#set( LIBS ${LIBS} ${Gsl_LIBRARIES} )

find_package( Glib REQUIRED )
include_directories( ${Glib_INCLUDE_DIRS} )
set( LIBS ${LIBS} ${Glib_LIBRARIES} )

#message (env var home = $ENV{HOME})

# Executable-prefix
set (_DEFAULT_EXE_PREFIX "gpiv_")
set(EXE_PREFIX "${_DEFAULT_EXE_PREFIX}"
  CACHE STRING "Prefix for application output-names")
mark_as_advanced(EXE_PREFIX)

#  --enable-mpi            enable Message Protocol Interface (MPI)
option( USE_MPI "Parallel processing on distributed memory systems" OFF )
if(USE_MPI)
  add_definitions(-DENABLE_MPI)
  find_package( MPI )
  include_directories( ${MPI_INCLUDE_PATH} )
  set( LIBS ${LIBS} ${MPI_LIBRARY} )
endif()

#  --enable-cam            enable (IEEE-1394) camera
option( USE_CAM "Use (IEEE-1394) camera software for image recording" OFF )
if(USE_CAM)
  add_definitions(-DENABLE_CAM)
endif()

#  --enable-trig           enable (realtime) triggering
option( USE_TRIG "Use RTAI software for laser and camera triggering" OFF )
if(USE_TRIG)
  add_definitions(-DENABLE_TRIG)
endif()

#  --disable-rta=RTATOPDIR     place where the RTAI code \
#resides (default /usr/lib/realtime)
#  --enable-k=KTOPDIR     place where the installed kernel \
#headers resides (default /usr/src/kernel-headers-2.4.27-adeos)

#  --enable-omp            enable Open Multi-Processing (OMP)
option( USE_OMP "Use Open Multi-Processing (OMP)" ON )
if(USE_OMP)
  add_definitions(-DENABLE_OMP)
endif()

# select debug behaviour and execution
option(DEBUG_V "Verbose behaviour for debugging." OFF)
if (DEBUG_V)
  add_definitions(-DDEBUG_V)
endif (DEBUG_V)

option(DISABLE_EXEC "Reduced execution for debugging." OFF)
if (DISABLE_EXEC)
  add_definitions(-DDISABLE_EXEC)
endif (DISABLE_EXEC)

# Man pages having identic prefix as executables
file(REMOVE_RECURSE ${CMAKE_BINARY_DIR}/man/)
set(DOC_PATH "share/man/man1")
install(FILES ${CMAKE_SOURCE_DIR}/man/gpivtools.1
        DESTINATION ${DOC_PATH}
)

## INCLUDE_DIRECTORIES($ENV{HOME}/include)

# Adding git hash
if (EXISTS ${CMAKE_SOURCE_DIR}/.git)
  option(USE_GIT_HASH "Show Git hash when -v | --version is used" ON)
  if (USE_GIT_HASH)
    find_package(Git)
    if (GIT_FOUND)
      add_definitions(-DGIT_HASH)
      git_hash ()
    else (GIT_FOUND)
      message ("No GIT executable is found. GIT_REVISION disabled")
      git_msg ("No git binary")
    endif (GIT_FOUND)
  else (USE_GIT_HASH)
    git_msg (Disabled)
  endif (USE_GIT_HASH)
else (EXISTS ${CMAKE_SOURCE_DIR}/.git)
  git_msg ("Disabled: no git repo")
endif (EXISTS ${CMAKE_SOURCE_DIR}/.git)

# Adding other configurations
configure_file (
  "${PROJECT_SOURCE_DIR}/config.h.in"
  "${PROJECT_BINARY_DIR}/config.h"
  )

# uninstall target
configure_file(
  "${CMAKE_SOURCE_DIR}/CMake/Modules/cmake_uninstall.cmake.in"
  "${CMAKE_BINARY_DIR}/cmake_uninstall.cmake"
  @ONLY
  )

add_custom_target(uninstall
  "${CMAKE_COMMAND}" -P "${CMAKE_BINARY_DIR}/cmake_uninstall.cmake"
  )

add_subdirectory (src)
