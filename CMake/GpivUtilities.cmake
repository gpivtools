function(gpiv_install_script name src)
  install (PROGRAMS ${src}
    RENAME ${EXE_PREFIX}${name}
    DESTINATION bin
  )
endfunction()


function(gpiv_add_executable name src)
  add_executable (${name} ${src})
  target_link_libraries (${name} ${GPIV_LIBRARIES} ${GSL_CONFIG_LIBS} m 
    ${Glib_LIBRARIES})
  set_target_properties (${name} PROPERTIES
    OUTPUT_NAME ${EXE_PREFIX}${name})
  install_targets (/bin ${name})

  add_dependencies(${name} githash)
  add_manpage(${name})
endfunction()


function(git_hash)
# Adding git hash
  set (gitrev_h git-rev.h)
  set (gitrev_in ${gitrev_h}.in)
  add_custom_target(githash
  ${CMAKE_COMMAND} -E remove -f ${CMAKE_CURRENT_BINARY_DIR}/${gitrev_h}
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${gitrev_in} ${CMAKE_CURRENT_BINARY_DIR}/${gitrev_h}
  COMMAND ${GIT_EXECUTABLE} rev-parse HEAD >> ${CMAKE_CURRENT_BINARY_DIR}/${gitrev_h}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}         #very important, otherwise git repo might not be found in shadow build
  VERBATIM                                              #portability wanted
  )
endfunction()


function(git_msg msg)
# Adding message instead of git hash
  set (gitrev_h git-rev.h)
  set (gitrev_in ${gitrev_h}.in)
  add_custom_target(gitrev
  ${CMAKE_COMMAND} -E remove -f ${CMAKE_CURRENT_BINARY_DIR}/${gitrev_h}
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/${gitrev_in} ${CMAKE_CURRENT_BINARY_DIR}/${gitrev_h}
  COMMAND echo ${msg} >> ${CMAKE_CURRENT_BINARY_DIR}/${gitrev_h}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}         #very important, otherwise git repo might not be found in shadow build
  VERBATIM                                              #portability wanted
  )
endfunction()


function(add_manpage name)
  file(COPY ${CMAKE_SOURCE_DIR}/man/${name}.1 
    DESTINATION ${CMAKE_BINARY_DIR}/man/)
  file(RENAME ${CMAKE_BINARY_DIR}/man/${name}.1 
    ${CMAKE_BINARY_DIR}/man/${EXE_PREFIX}${name}.1)

  install(FILES ${CMAKE_BINARY_DIR}/man/${EXE_PREFIX}${name}.1
        DESTINATION ${DOC_PATH}
)


endfunction()